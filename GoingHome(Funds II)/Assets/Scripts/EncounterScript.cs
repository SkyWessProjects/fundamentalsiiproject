using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterScript : MonoBehaviour
{
    public BattleHudManager hud;

    public GameObjectStats[] players;
    public int playerIndex = 0;
    public GameObjectStats[] enemies;
    public int enemyIndex = 0;
    private void Update()
    {
        UpdateTimers();
        CheckForTurns();
    }
    private void OnTriggerEnter(Collider other)
    {
        hud.hudCanvas.SetActive(true);
        if (other.gameObject.tag == "Player")
        {
            hud.hudCanvas.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        hud.hudCanvas.SetActive(false);
        if (other.gameObject.tag == "Player")
        {
            hud.hudCanvas.SetActive(false);
        }
    }
    public void UpdateTimers()
    {
        foreach (GameObjectStats player in players)
        {
            if (!player.stats.hasTurn && !player.stats.stunned)
                player.stats.turnTimer -= Time.deltaTime * (player.stats.currentSpeed * .01f);
            if (player.stats.turnTimer <= 0)
            {
                player.stats.hasTurn = true;
                player.stats.turnTimer = player.stats.turnTime;
            }
            player.stats.StatisEffects(0);
        }

        foreach (GameObjectStats enemy in enemies)
        {
            if (!enemy.stats.hasTurn && !enemy.stats.stunned)
                enemy.stats.turnTimer -= Time.deltaTime * (enemy.stats.currentSpeed * .01f);
            if (enemy.stats.turnTimer <= 0)
            {
                enemy.stats.hasTurn = true;
                enemy.stats.turnTimer = enemy.stats.turnTime;
            }
            enemy.stats.StatisEffects(0);
        }
    }

    public void CheckForTurns()
    {
        foreach (GameObjectStats player in players)
        {
            if (player.stats.hasTurn == true)
            {
                hud.EnableButtonsOnTurn(player.stats);
            }
        }
    }

}
