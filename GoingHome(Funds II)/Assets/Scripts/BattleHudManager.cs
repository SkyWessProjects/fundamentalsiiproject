using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleHudManager : MonoBehaviour
{
    public GameObject hudCanvas;
    public TextMeshProUGUI characterNameText;
    public GameObject targetMenu;
    public GameObject abilityMenu;
    public Button buttonTemplate;
    public Button[] baseMenuButtons;
    public Button[] targetMenuButtons;
    public Button[] abilityMenuButtons;
    private Stats currentPlayer;


    public void EnableButtonsOnTurn(Stats character) 
    {
        currentPlayer = character;
        characterNameText.text = character.playerName;
        if(character.hasTurn == true)
        {
            foreach(Button button in baseMenuButtons)
            {
                button.GetComponent<Button>().interactable = true;
            }
        }
        else
        {
            foreach (Button button in baseMenuButtons)
            {
                button.GetComponent<Button>().interactable = false;
            }
        }
    }
    void TargetSelect(GameObjectStats player, GameObjectStats a)
    {
        player.gameObject.GetComponent<MeleeAttack>().target = a;
    }
    public void HandleTargetMenu(GameObjectStats player, GameObjectStats[] character)
    {
        int i = 0;
        foreach (GameObjectStats a in character)
        {
            targetMenuButtons[i] = Instantiate<Button>(buttonTemplate);
            targetMenuButtons[i].GetComponent<TextMeshProUGUI>().text = a.stats.playerName;
            targetMenuButtons[i].GetComponent<Button>().onClick.AddListener(delegate { TargetSelect(player, a); });
            i++;
        }
    }
    public void HandleAbilityMenu()
    {
        int i = 0;
        foreach(Ability a in currentPlayer.playerAbilities)
        {
            abilityMenuButtons[abilityMenuButtons.Length] = Instantiate<Button>(buttonTemplate);
            abilityMenuButtons[abilityMenuButtons.Length].GetComponent<TextMeshProUGUI>().text = a.abilityName;
            i++;
        }
    }
}