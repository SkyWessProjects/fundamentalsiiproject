using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardUI : MonoBehaviour
{
    public Camera cam;
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(cam.transform.position);
    }
}
