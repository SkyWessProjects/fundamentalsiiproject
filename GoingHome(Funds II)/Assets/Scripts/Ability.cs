using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ability", menuName = "Ability", order = 2)]
public class Ability : ScriptableObject 
{ 
    public string abilityName;
    public int MPCost;

    public ParticleSystem particleEffect;
    public GameObject statisEffectIcon;

    public int abilityTurnLength;

    public float damage;
    public int stun;
    public float statChange;


}
