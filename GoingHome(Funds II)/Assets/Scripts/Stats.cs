using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Stats", menuName = "Character Stats", order = 1)]
public class Stats : ScriptableObject
{                 
    public string playerName = "";
    public bool isAlive;
    public Ability[] playerAbilities;

    public GameObject playerUI;
    [Header("Health + Mana")]
    public float totalHP;
    public float currentHP;
    public Image hpBar;

    public float totalMP;
    public float currentMP;

    [Header("General Stats")]
    public float totalStr;
    public float currentStr;

    public float totalMag;
    public float currentMag;

    public float totalStrDef;
    public float currentStrDef;

    public float totalMagDef;
    public float currentMagDef;

    public float totalHitRate;
    public float currentHitRate;

    public float totalSpeed;
    public float currentSpeed;

    [Header("Statis Effects")]
    public bool dot;
    public bool stunned;
    public bool statChange;

    [Header("Timers")]
    public bool hasTurn;
    public float turnTime;
    public float turnTimer;
    public float statisEffectTimer;
    //Controls for the character's health and mana
    public void LooseHP(float damage)
    {
        currentHP -= damage;
        if (currentHP < 0)
            currentHP = 0;
    }
    public void GainHP(float heal)
    {
        if (currentHP < 0)
            isAlive = true;
        currentHP += heal;
        if (currentHP > totalHP)
            currentHP = totalHP;
    }

    public void LooseMP(float cost)
    {
        currentMP -= cost;
        if (currentMP <= 0)
        {
            currentMP = 0;
            isAlive = false;
        }
    }
    public void GainMP(float gain)
    {
        currentMP += gain;
        if (currentMP > totalMP)
            currentMP = totalMP;
    }

    //Updates HP, MP, and Statis Effect Symbols
    public void UpdateStatisBars()
    {
        hpBar.fillAmount = totalHP/currentHP;
    }

    //Deal damage to the targeted enemy based on what kind of attack it is.
    public void DoDmgStr(Stats enemy)
    {
        enemy.LooseHP(currentStr * .2f);
    }
    public void DoDmgMag(Stats enemy)
    {
        enemy.LooseHP(currentMag * .2f);
    }

    //Deal with status conditions if they are affecting the player.
    public void StatisEffects(float damage)
    {
        if (dot || stunned || statChange && statisEffectTimer <= 0)
      {
            dot = false;
            stunned = false;
            statChange = false;
            statisEffectTimer = 0;
        }
        if (dot)
        {
             currentHP -= currentHP * damage;
        }
        if (stunned)
        {
            //Code to stop the player turntimer from counting down.
        }
    }
}