using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMovement : MonoBehaviour
{
    bool isMoving;
    public float moveSpeed = 2.5f;
    public float snapToDistance = 0.5f;
    Vector3 targetPosition;
    Vector3 startPosition;

    public float rayLength = 1.2f;
    public float rOffsetX = 0.5f;
    public float rOffsetY = 0.5f;
    public float rOffsetZ = 0.5f;

    Vector3 xOffset;
    Vector3 yOffset;
    Vector3 zOffset;
    Vector3 zAxisOriginA;
    Vector3 zAxisOriginB;
    Vector3 xAxisOriginA;
    Vector3 xAxisOriginB;

    public Transform cameraRotator;
    public LayerMask walkableMask = 0;

    public LayerMask collidableMask = 0;

    public float maxFallCastDistance = 100f;
    public float fallSpeed = 30f;
    bool isFalling;
    float targetFallHeight;

    // Update is called once per frame
    void Update()
    { // Set the ray positions every frame
        yOffset = transform.position + Vector3.up * rOffsetY;
        zOffset = Vector3.forward * rOffsetZ;
        xOffset = Vector3.right * rOffsetX;

        zAxisOriginA = yOffset + xOffset;
        zAxisOriginB = yOffset - xOffset;

        xAxisOriginA = yOffset + zOffset;
        xAxisOriginB = yOffset - zOffset;
        if (isFalling)
        {
            if (transform.position.y <= targetFallHeight)
            {
                float x = Mathf.Round(transform.position.x);
                float y = Mathf.Round(targetFallHeight);
                float z = Mathf.Round(transform.position.z);

                transform.position = new Vector3(x, y, z);

                isFalling = false;

                return;
            }

            transform.position += Vector3.down * fallSpeed * Time.deltaTime;
            return;
        }
        else if (isMoving)
        {
            if (Vector3.Distance(startPosition, transform.position) > 1f)
            {
                float x = Mathf.Round(targetPosition.x);
                float y = Mathf.Round(targetPosition.y);
                float z = Mathf.Round(targetPosition.z);

                transform.position = new Vector3(x, y, z);

                isMoving = false;

                return;
            }

            transform.position += (targetPosition - startPosition) * moveSpeed * Time.deltaTime;
            return;
        }
        else
        {
            RaycastHit[] hits = Physics.RaycastAll(
                    transform.position + Vector3.up * 0.5f,
                    Vector3.down,
                    maxFallCastDistance,
                    walkableMask
            );

            if (hits.Length > 0)
            {
                int topCollider = 0;
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[topCollider].collider.bounds.max.y < hits[i].collider.bounds.max.y)
                        topCollider = i;
                }
                if (hits[topCollider].distance > .8f)
                {
                    targetFallHeight = transform.position.y - hits[topCollider].distance + 0.5f;
                    isFalling = true;
                }
            }
            else
            {
                targetFallHeight = -Mathf.Infinity;
                isFalling = true;
            }
        }

        // Handle player input
        // Also handle moving up 1 level

        if (Input.GetKey(KeyCode.W) && isMoving == false && isFalling == false)
        {
            Move(cameraRotator.transform.forward, false, "W");
        }
        else if (Input.GetKey(KeyCode.S) && isMoving == false && isFalling == false)
        {
            Move(-cameraRotator.transform.forward, true, "S");
        }
        else if (Input.GetKey(KeyCode.A) && isMoving == false && isFalling == false)
        {
            Move(-cameraRotator.transform.right, true, "A");
        }
        else if (Input.GetKey(KeyCode.D) && isMoving == false && isFalling == false)
        {
            Move(cameraRotator.transform.right, false, "D");
        }
    }
    void Move(Vector3 direction, bool subtract, string key)
    {
        if(CanMove(direction))
        {
            if (key == "W")
                targetPosition = transform.position + cameraRotator.transform.forward;
            else if (key == "S")
                targetPosition = transform.position - cameraRotator.transform.forward;
            else if (key == "A")
                targetPosition = transform.position - cameraRotator.transform.right;
            else if (key == "D")
                targetPosition = transform.position + cameraRotator.transform.right;
            startPosition = transform.position;
            isMoving = true;
        }
        else if (CanMoveUp(direction))
        {
            if(key == "W")
                targetPosition = transform.position + cameraRotator.transform.forward + Vector3.up;
            else if (key == "S")
                targetPosition = transform.position - cameraRotator.transform.forward + Vector3.up;
            else if (key == "A")
                targetPosition = transform.position - cameraRotator.transform.right + Vector3.up;
            else if (key == "D")
                targetPosition = transform.position + cameraRotator.transform.right + Vector3.up;
            startPosition = transform.position;
            isMoving = true;
        }
    }
    bool CanMove(Vector3 direction)
    {
        if (direction.z != 0)
        {
            if (Physics.Raycast(zAxisOriginA, direction, rayLength))
                return false;
            if (Physics.Raycast(zAxisOriginB, direction, rayLength))
                return false;
        }
        else if (direction.x != 0)
        {
            if (Physics.Raycast(xAxisOriginA, direction, rayLength))
                return false;
            if (Physics.Raycast(xAxisOriginB, direction, rayLength))
                return false;
        }
        return true;
    }
    // Check if the player can step-up

    bool CanMoveUp(Vector3 direction)
    {
        if (Physics.Raycast(transform.position + Vector3.up * 0.5f, Vector3.up, 1f, collidableMask))
            return false;
        if (Physics.Raycast(transform.position + Vector3.up * 1.5f, direction, 1f, collidableMask))
            return false;
        if (Physics.Raycast(transform.position + Vector3.up * 0.5f, direction, 1f, walkableMask))
            return true;
        return false;
    }

    void OnCollisionEnter(Collision other)
    {
        if (isFalling && (1 << other.gameObject.layer & walkableMask) == 0)
        {
            // Find a nearby vacant square to push us on to
            Vector3 direction = Vector3.zero;
            Vector3[] directions = { Vector3.forward, Vector3.right, Vector3.back, Vector3.left };
            for (int i = 0; i < 4; i++)
            {
                if (Physics.OverlapSphere(transform.position + directions[i], 0.1f).Length == 0)
                {
                    direction = directions[i];
                    break;
                }
            }
            transform.position += direction;
        }
    }

}
