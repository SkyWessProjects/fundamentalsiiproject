using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    private Stats stats;
    public GameObjectStats target;
    public float speed;
    private void Awake()
    {
        stats = gameObject.GetComponent<GameObjectStats>().stats;
    }
    private void Update()
    {
        if (target != null)
        ApproachTarget(target);
    }
    public void ApproachTarget(GameObjectStats target)
    {

        float step = speed * Time.deltaTime;
        gameObject.transform.position = Vector3.MoveTowards(transform.position, target.gameObject.transform.position, step);
    }
    public void HitTarget(GameObjectStats target)
    {
        stats.DoDmgStr(target.stats);
    }
}
