using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlayer : MonoBehaviour
{
    public GameObject[] characters;
    public GameObject activeCharacter;
    public int characterCounter;

    private void Start()
    {
        activeCharacter = characters[0];
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            activeCharacter.GetComponent<GridMovement>().enabled = false;
            GetNextCharacter();
            activeCharacter.GetComponent<GridMovement>().enabled = true;

        }
    }
    private void GetNextCharacter()
    {
        characterCounter++;
        if (characterCounter >= characters.Length)
            characterCounter = 0;
        activeCharacter = characters[characterCounter];
    }
}
