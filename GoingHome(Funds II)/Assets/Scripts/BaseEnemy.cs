using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyActions
{
    Attack,
    Ability,
}
public class BaseEnemy : MonoBehaviour
{
    public EnemyActions[] actionList;
    public Stats enemyStats;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
