using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class PauseScript : MonoBehaviour
{
    private bool gameIsPaused = false;
    public GameObject panel;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && gameIsPaused == false)
        {
            Time.timeScale = 0;
            panel.SetActive(true);
            gameIsPaused = true;
            Debug.Log("Pause");
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && gameIsPaused == true)
        {
            Resume();
            Debug.Log("UnPause");
        }
    }

    public void Resume()
    {
        Time.timeScale = 1;
        panel.SetActive(false);
        gameIsPaused = false;
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("quittin time bois");
    }
}
